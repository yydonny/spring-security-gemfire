
Title
=========

## Run the demo

First, start a local Gemfire locator.

```powershell
$host_name = 'USTX15369L01658.nam.nsroot.net'
gfsh start locator --name=locator1 --mcast-port=0 --port=10086
gfsh start locator --name=locator2 --mcast-port=0 --port=10010 --locators="'$host_name[10086]'"

# gfsh -e "connect --locator='$host_name[10086]'" -e "list members"
# gfsh -e "connect --locator='$host_name[10086]'" -e "shutdown --include-locators=true"
```

Build the common module.

```
.\sbin\update-commons.cmd
```

Then start all the components.

```powershell
 .\sbin\cache-server.cmd
 .\sbin\content-server.cmd
 .\sbin\resource-server.cmd
 .\sbin\auth-server.cmd
```

After that, point browser to `http://localhost:8080/ui/`, and then login with `user/asdf1234` for normal user, `admin/asdf1234` for admin.

```

$ http localhost:26510/user
HTTP/1.1 403 Forbidden
Content-Type: application/json;charset=utf-8
Date: Wed, 18 Oct 2017 20:18:11 GMT
Pragma: no-cache
Set-Cookie: XSRF-TOKEN=5bbb5e46-5ca3-4be4-b1ef-9a721d9c3b3b;Path=/
Transfer-Encoding: chunked
X-Content-Type-Options: nosniff
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block
x-auth-token: 873f2cf3-7525-4deb-8beb-05be548012be

{
    "error": "Forbidden",
    "message": "Access Denied",
    "path": "/user",
    "status": 403,
    "timestamp": 1508357891847
}





```

## Ochestration

Auth server is now the gateway of UI.

Access control is centralized to Auth server.

Content server works behind the Auth server, and is no longer secured by itself.

Resource server is still secured by shared session, and is accessed directly by the client.

- auth-server (the gateway): `8080`
- content-server: `26511`
- resource-server: `26510`

## Component

### Auth Server


### Resource Server


### Content Server

### Cache Server

A dedicated Gemfire cache server for clustered Spring session.

### Commons

This module holds common components shared by other modules.

If you run services that depend on `commons` with `mvn spring-boot:run`, it will look for the installed jar of `commons` 
in the local Maven repository. So it is necessary to update `commons` module in the local repository after any change to it 
before you run other services with the `spring-boot:run` goal in development. (Note this is not needed for building the executable jar)

There is a convenient script `sbin\update-commons.ps1` for this purpose.

#### Gemfire Cache

Configuration classes for establishing a Gemfire peer cache server. To use:

1. enable `gemfire-cache` profile
2. add `@EnableGemfireCache` annocation
3. add `gemfire.cache.peerConfig` properties




## Gemfire Setup

This project uses Gemfire as caching provider. Thus a Gemfire locator has to be available for this setup.
You can launch one locally following the starting steps of this [gemfire tutorial].
Also here is a [gemfire example].

> **Log4j Dependency Problem**

> Gemfire library expects log4j-api and log4j-core available in local CLASSPATH, otherwise it will crash on startup.
> A workaround of this problem is to copy jars come from GemFire installation to the Maven repository cache:

```Powershell
cp $env:GEMFIRE\lib\log4j-* $home\.m2\repository\com\gemstone\gemfire\gemfire\8.2.4\

# Remove conflicting slf4j jar
rm $home\.m2\repository\com\gemstone\gemfire\gemfire\8.2.4\*slf4j*
```

[gemfire example]: https://github.com/jxblum/spring-boot-gemfire-server-example
[gemfire tutorial]: http://gemfire82.docs.pivotal.io/docs-gemfire/getting_started/15_minute_quickstart_gfsh.html
