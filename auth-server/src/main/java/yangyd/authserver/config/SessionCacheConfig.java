package yangyd.authserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.gemfire.config.annotation.web.http.EnableGemFireHttpSession;
import org.springframework.session.web.http.CookieHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;
import yangyd.commons.EnableGemfireCache;

@Configuration
@EnableGemFireHttpSession
@EnableGemfireCache
class SessionCacheConfig {
  @Bean
  HttpSessionStrategy httpSessionStrategy() {
//    return new HeaderHttpSessionStrategy();
    return new CookieHttpSessionStrategy();
  }
}
