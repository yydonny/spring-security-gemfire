package yangyd.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.stereotype.Component;

@EnableWebSecurity
public class WebSecurityConfig {

  @Autowired
  public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
        .withUser("user").password("asdf1234").roles("USER")
        .and()
        .withUser("admin").password("asdf1234").roles("USER", "ADMIN", "READER", "WRITER")
        .and()
        .withUser("audit").password("asdf1234").roles("USER", "ADMIN", "READER");
  }

  @Component
  @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
  static class O extends WebSecurityConfigurerAdapter {
    /**
     * Spring security example: Http Basic login with session support
     *
     * Request by a valid principal (identified by JSESSIONID) is allowed without "authorization:Basic " header
     *
     * There's no specific '/login' endpoint, any request with correct "authorization:Basic " header
     * will establish a logged-in session.
     *
     * A `/user` endpoint is exposed for the client to query current principal.
     * This solution also solves the 'unable-to-logout' problem of Http Basic authorization
     *
     * Post to /logout invalidates the JSESSIONID (thus the session and principal information).
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
      // Spring Security recognizes the "X-Requested-With: XMLHttpRequest" header of AJAX request
      // by not sending a "WWW-Authenticate" header in a 401 response, and thus the browser will
      // not pop up an authentication dialog
      http.httpBasic().realmName("Kalimdor")
          .and()

          .authorizeRequests()
          .antMatchers("/ui/user/**").hasRole("USER")
          .antMatchers("/ui/admin/**").hasRole("ADMIN")
          .antMatchers("/ui/**").permitAll()
          .anyRequest().authenticated()
          .and()

          // Use cookie to send csrf token
          // The default cookie name happens to be the same as what Angular recognizes.
          .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
  }
}
