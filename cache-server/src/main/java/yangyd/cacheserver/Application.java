package yangyd.cacheserver;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.gemfire.config.annotation.web.http.EnableGemFireHttpSession;
import yangyd.commons.EnableGemfireCache;

@SpringBootApplication
@EnableGemFireHttpSession
@EnableGemfireCache
class Application {
}
