package yangyd.commons;

import org.springframework.context.annotation.Import;
import org.springframework.integration.config.EnableIntegration;
import yangyd.commons.cache.GemfireCacheConfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to activate the Gemfire peer cache.
 *   - activate the 'gemfire-cache' profile
 *   - provide peer configuration.
 *
 * @see GemfireCacheConfig
 * @see yangyd.commons.cache.GemfireCacheProps
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@EnableIntegration
@Import(GemfireCacheConfig.class)
public @interface EnableGemfireCache {
}
