package yangyd.commons.cache;

import com.gemstone.gemfire.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.gemfire.CacheFactoryBean;
import org.springframework.data.gemfire.support.GemfireCacheManager;

@Configuration
@EnableConfigurationProperties(GemfireCacheProps.class)
@Profile("gemfire-cache")
public class GemfireCacheConfig {
  @Autowired
  private GemfireCacheProps props;

  @Bean
  GemfireCacheManager cacheManager(Cache gemfireCache) {
    GemfireCacheManager cacheManager = new GemfireCacheManager();
    cacheManager.setCache(gemfireCache);
    return cacheManager;
  }

  @Bean
  CacheFactoryBean gemfireCache() {
    CacheFactoryBean gemfireCache = new CacheFactoryBean();
    gemfireCache.setClose(true);
    gemfireCache.setProperties(props.getPeerConfig());
    return gemfireCache;
  }
}
