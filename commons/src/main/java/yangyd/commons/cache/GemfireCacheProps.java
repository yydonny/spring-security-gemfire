package yangyd.commons.cache;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

/**
 * Provides configuration properties of a Gemfire cache peer.
 * @see <a href="http://gemfire82.docs.pivotal.io/docs-gemfire/latest/topologies_and_comm/p2p_configuration/setting_up_a_p2p_system.html">http://gemfire82.docs.pivotal.io/docs-gemfire/latest/topologies_and_comm/p2p_configuration/setting_up_a_p2p_system.html</a>
 */
@ConfigurationProperties("gemfire.cache")
class GemfireCacheProps {
  private final Properties peerConfig = new Properties();

  // must be public for config injection
  public Properties getPeerConfig() {
    return peerConfig;
  }
}
