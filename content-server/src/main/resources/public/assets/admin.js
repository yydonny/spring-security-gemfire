
;(function(window, angular) {

  angular.module('admin', [])
    .controller('home', function ($http) {
      $http.get('/user').then(response => {
        var data = response.data;
        if (data.name) {
          this.authenticated = true;
          this.user = data;
          this.template = rw(this.user);
        } else {
          this.authenticated = false;
        }
      }, () => {
        this.authenticated = false;
      });
    });


  function rw(user) {
    return (user && user.roles && ~user.roles.indexOf("ROLE_WRITER")) ?
      "write.html" : "read.html";
  }

}(window, window.angular));


