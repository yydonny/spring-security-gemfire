angular.module('hello', [ 'ngRoute' ])
  .config(function($routeProvider, $httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
  })

  .controller('navigation', function($rootScope, $http, $location) {
    var self = this;

    var authenticate = (credentials, callback) => {
      var login_header = credentials ? {
            authorization : "Basic " + btoa(credentials.username + ":" + credentials.password)
        } : {};
      this.user = '';

      $http.get('/user', {headers: login_header})
        .then(response => {
          var data = response.data;
          if (data.name) {
            this.authenticated = true;
            this.user = data.name;
            this.admin = data && data.roles && ~data.roles.indexOf("ROLE_ADMIN");
          } else {
            this.authenticated = false;
            this.admin = false;
          }
          callback && callback(true);

        }, () => {
          this.authenticated = false;
          callback && callback(false);
        });
    };

    authenticate();

    console.log(this.user);

    // clear cached login credentials on each navigation
    self.credentials = {};

    this.login = () => {
      authenticate(this.credentials, authenticated => {
        self.authenticated = authenticated;
        self.error = !authenticated;
      });
    };

    // POST to /logout to logout (erase the principal on server-side)
    self.logout = function() {
      $http.post('logout', {}).finally(() => {
        this.authenticated = false;
        this.admin = false;
      });
    };
  });

