package yangyd.resourceserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.session.data.gemfire.config.annotation.web.http.EnableGemFireHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;
import org.springframework.stereotype.Component;
import yangyd.commons.EnableGemfireCache;

@Configuration
@EnableGemFireHttpSession
@EnableGemfireCache
class WebSecurityConfig {

  @Bean
  HttpSessionStrategy httpSessionStrategy() {
    return new HeaderHttpSessionStrategy();
//    return new CookieHttpSessionStrategy();
  }

  @Component
  static class Config extends WebSecurityConfigurerAdapter {
    /**
     * Allow access with a valid session ID (in the X-Auth-Token header)
     * Also should config spring not to create session (only use shared one):
     *   security.sessions: never
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http.cors() // allow CORS request
        .and()
          .authorizeRequests()
          .anyRequest().authenticated()
        .and()
          .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
  }
}
