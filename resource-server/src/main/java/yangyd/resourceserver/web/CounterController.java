package yangyd.resourceserver.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/counter")
class CounterController {

  @RequestMapping("/init")
  ResponseEntity<?> init(HttpSession session) {
    session.setAttribute("counter", new AtomicLong(0));
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @RequestMapping("/inc")
  ResponseEntity<?> increment(HttpSession session) {
    AtomicLong counter = (AtomicLong) session.getAttribute("counter");
    if (counter == null) {
      throw new IllegalStateException();
    } else {
      counter.incrementAndGet();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

  @RequestMapping("/get")
  Map<String, Long> get(HttpSession session) {
    AtomicLong counter = (AtomicLong) session.getAttribute("counter");
    if (counter == null) {
      throw new IllegalStateException();
    } else {
      return Collections.singletonMap("counter", counter.get());
    }
  }

  @ExceptionHandler
  ResponseEntity<?> on(IllegalStateException ise) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }
}
