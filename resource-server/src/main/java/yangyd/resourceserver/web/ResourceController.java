package yangyd.resourceserver.web;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import yangyd.resourceserver.model.Message;

import java.security.Principal;

@RestController
@RequestMapping("/")
public class ResourceController {

  @RequestMapping("/user")
  Principal user(Principal user) {
    return user;
  }

  @RequestMapping("/message")
  @CrossOrigin(origins="*", maxAge=3600,
      allowedHeaders={"x-auth-token", "x-requested-with"})
  public Message home() {
    return new Message("a message!");
  }
}
